# Iago Olímpio Frota

Desenvolvedor júnior PHP, ASP.NET e Javascript. Está adorando o seu Arduino.

| <iagofrota10@hotmail.com> |
[Github](https://github.com/iagofrota) |
[Linkedin](https://www.linkedin.com/in/iagofrota/)

## Sobre
Sou um desenvolvedor júnior participando de projetos desafiadores utilizando PHP 7 e Scrum. Atualmente, estou participando de uma startup chamada [Vitrine](https://goo.gl/U1qtUk) onde sou um dos desenvolvedores e também tento desenvolver alguns projetos pessoais onde utilizo *Domain Driven Design* e *Test Driven Design*. Uso meu tempo livre para fazer coisas legais com Arduino e praticar meu Jiu-jitsu.

## Projetos
### [Vitrine](http://minhavitrine.net/)
Central de Serviços é uma solução digital criada para aproximar profissionais liberais de diversos segmentos aos seus potenciais clientes de forma com que eles possam ter uma comunicação fácil, rápida e direta, fortalecida com base na flexibilidade do atendimento em domicílio e indicações de outros consumidores em um ambiente confiável e seguro que possibilita várias formas de negociação e pagamento, inclusive troca entre serviços/produtos.

Funciona através de uma plataforma web e diferencia-se por manter um guia informativo com as principais tendências de mercado, agendamento online e área administrativa para profissionais.

É a tecnologia adaptada para que todos possam dispor dos melhores serviços com os melhores profissionais, seja onde for.

### Controle Interno para Gestão Pública
Esse projeto é de minha autoria. Trabalhei algum tempo na gestão pública, na parte de controle interno, e senti uma falta muito grande de uma ferramenta que me assessorasse, melhor, no meu trabalho. Ainda estou desenvolvendo nas horas
livres. Utilizo ASP.NET MVC 5, SQLServer, Entity Framework 6. Em termos arquiteturais, tento seguir as metodologias do *Domain Driven Design* (DDD) e *Test Driven Design* (TDD).

### [Licitação de Sobral](http://ici.sobral.org/licitacao/) | [ICI](https://goo.gl/Er4JCg)
Foi um projeto para criar uma solução para o processo de licitação da [Prefeitura de Sobral](http://www.sobral.ce.gov.br/site_novo/). Antes, o controle era feito utilizando planilhas eletrônicas, onde muitas pessoas utilizavam. Utilizamos PHP 7, Javascript, MySQL e *flat design* como soluções técnicas. Utilizamos Scrum como metodologia de planejamento e gestão de projetos.

### CIOPS de Sobral | [ICI](https://goo.gl/Er4JCg)
Foi uma demanda para modernizar o sistema atual de cadastro de ocorrências. Nossa solução foi criar um sistema web e que, principalmente, desse suporte a relatórios que servissem para planejar melhor as estratégias de ações públicas. Como soluções tecnológicas, utilizamos PHP7, javascript e MySQL.

### [Portal da Transparência de Sobral](https://goo.gl/GhG1Cq) | [ICI](https://goo.gl/Er4JCg)
Foi uma demanda da Secretaria de Gestão da Prefeitura de Sobral para modernizar e adequar o Portal da Transparência de Sobral aos requisitos do Tribunal de Contas do Estado - TCE. Tentamos utilizar *flat design* para um melhor experiência dos usuários. Utilizamos PHP 7 e MySQL.

### Viver Sem Limites | [e-Jovem](https://goo.gl/PBr1nv)
É um Projeto Social onde fui líder/apoiador do Projeto Viver sem Limites em Massapê. Esse projeto de empreendedorismo social fazia parte da grade curricular do alunos do [Projeto E-Jovem](https://goo.gl/PBr1nv). Nesse projeto, nós procuramos despertar à Comunidade Massapeense o pleno exercício dos direitos individuais e sociais das pessoas com deficiência. Conseguimos treinar um grupo de pessoas a linguagem Libras.

## Publicações
### [Associação por chave estrangeira no Entity Framework](https://goo.gl/av6ruf)

## Experiência
### Desenvolvedor/Analista Júnior | [Prefeitura de Sobral](http://www.sobral.ce.gov.br/site_novo/) | outubro 2017 até o momento

### Desenvolvedor/Analista | [Instituto de Comunicação e Informática - ICI](http://ici.sobral.org/) | julho 2017 até setembro 2017
Desempenhei o papel de analista de testes, analista de levantamento de requisitos e desenvolvedor júnior nos seguintes projetos:
- [Sistema de Licitação da Prefeitura de Sobral](https://goo.gl/aNuit1)
- [CIOPS](https://goo.gl/5Qc4X1)
- [Portal da Transparência da Sobral](https://goo.gl/Bmeyut)
- Site Institucional do ICI

Sempre procuramos desenvolver nossas soluções tentando seguir padrões de arquitetura design de *software*. Como metodologia de projetos, utilizavamos o Scrum e como solução tecnológica PHP7, javascript e MySQL.

### Assistente Técnico de Controle Interno | Secretaria Municipal da Saúde de Massapê | junho 2015 até dezembro de 2016
- Responsável pela gestão dos contratos dos fornecedores,
- Acompanhar o vencimento dos contratos,
- Verificar as prestações de conta junto com Secretário da Saúde,
- Acompanhar e Verificar as licitações solicitadas.

Já no software de Controle Interno:
- Responsável por cadastrar nossas ordens de compra e de serviço,
- Responsável por cadastrar os fornecedores,
- Controlar as entradas e as saídas das mercadorias,
- Acompanhar a Posição Geral do estoque de mercadorias,
- Controlar e verificar o saldo das licitações e aditivos. 

Em 2015, fui designado pelo Secretário da Saúde de Massapê, por meio da Portaria Nº 20/2015, para exercer as atividades de Assistente Técnico de Controle Interno na Secretaria Municipal da Saúde de Massapê e, no mesmo ano, recebi um reconhecimento pelo trabalho desenvolvido com dedicação.

### Auxiliar de Faturamento | Secretaria Municipal da Saúde de Massapê | janeiro 2013 a junho 2015
Fui responsável por fazer atualizações e acompanhar o quadro de funcionários da área médica tanto dos Centro de Saúde da Família como do Hospital Municipal. Fui, também, responsável por digitar e faturar as internações que aconteceram no hospital de Massapê.

### Estágio de Monitor e Educador do Projeto e-Jovem | Secretaria da Educação do Estado do Ceará | outubro 2012 até março 2013
Quatro dias da semana eu era responsável por monitorar as atividades on-line dos alunos. E um dia da semana eu era responsável por ministrar uma disciplina chamada de Preparação para o Trabalho e Prática Social que tinha 12 temáticas: 

- Introdução a empregabilidade, 
- Postura Sócio Profissional, 
- Empreendedorismo, 
- Identidade, 
- Cidadania, 
- Comunicação e 
- Outras. 

Minha equipe e eu fomos responsáveis por criar um [projeto social](https://goo.gl/aE1WJ7) que tem como objetivo auxiliar a inclusão de pessoas com deficiência na sociedade de Massape.

## Escolaridade
### MBA em Engenharia de Software com Métodos Ágeis | [IGTI](http://igti.com.br/) | 2016 - 2017 (em andamento)

### Tecnólogo em Análise e Desenvolvimento de Sistemas | [UNOPAR](http://www.unoparead.com.br/) | 2013 - 2016 (concluída)

### Bacharelado em Ciências da Computação | [Universidade Estadual do Vale do Acaraú](http://www.uvanet.br/) | 2011 - 2013 (incompleta)

## Idiomas
### Inglês
Intermediário para leitura e básico para conversação.

## Cursos e Treinamentos
### Desenvolvedor Javascript | [Alura](https://www.alura.com.br/) | [Certificado](https://cursos.alura.com.br/career/certificate/38ee7202-8b2b-49f6-a4a0-a62b40292bc5)

### Criando Domínio Ricos | [BaltaIO]() | [Certificado]()

### Criando aplicações escaláveis com arquiteturas modernas | [BaltaIO]() | [Certificado]()


<br>

[Versão em PDF](https://gitprint.com/iagofrota/meu-curriculo?print)
